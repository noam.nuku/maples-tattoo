import { createApp } from "vue";
import App from "./App.vue";
import BaseSection from "./components/UI/BaseSection.vue";
import BaseScrollAnimation from "./components/UI/BaseScrollAnimation.vue";

// import 3rd party plugin
import VueKinesis from "vue-kinesis";

// css
import "./assets/scss/app.scss";

const app = createApp(App);
import AOS from "aos";
import "aos/dist/aos.css";
app.AOS = new AOS.init();
app.component("base-section", BaseSection);
app.component("base-scroll-animation", BaseScrollAnimation);

// use 3rd party plugin
app.use(VueKinesis);
app.mount("#app");
